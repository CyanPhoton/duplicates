#ifndef DUPLICATES_HASH_MAP_H
#define DUPLICATES_HASH_MAP_H

#include <stdbool.h>
#include "vector.h"

typedef const byte *(*KeyAccessor)(const void *value);

#ifdef HASH_MAP_INTERNAL
struct HashMap {
    Vec bins;
    size_t key_size;
    size_t value_size;
    size_t bin_count_index;
    size_t value_count;
    KeyAccessor key_accessor;
    size_t largest_bin;
    VecElementDestructor value_destructor;
} typedef HashMap;

struct HashMapIter {
    const HashMap *hashMap;
    VecIter bins;
    VecIter entries;
} typedef HashMapIter;

struct HashMapIterMut {
    HashMap *hashMap;
    VecIterMut bins;
    VecIterMut entries;
} typedef HashMapIterMut;
#else
struct HashMap {
    Vec _bins;
    size_t _key_size;
    size_t _value_size;
    size_t _bin_count_index;
    size_t _value_count;
    KeyAccessor _key_accessor;
    size_t _largest_bin;
    VecElementDestructor _value_destructor;
} typedef HashMap;

struct HashMapIter {
    const HashMap* _hashMap;
    VecIter _bins;
    VecIter _entries;
} typedef HashMapIter;

struct HashMapIterMut {
    HashMap* _hashMap;
    VecIterMut _bins;
    VecIterMut _entries;
} typedef HashMapIterMut;
#endif

// In this configuration, a value contains the key and set_key_accessor says how to access the key given a value
extern HashMap hash_map_init(size_t key_size, size_t value_size, KeyAccessor key_accessor, VecElementDestructor value_destructor);

// Returns true if there was already a value with the same key
extern bool hash_map_insert(HashMap *self, const byte *value);
// Returns NULL if no value with that key
extern const byte *hash_map_get_value(const HashMap *self, const byte *key);
// Returns NULL if no value with that key
extern byte *hash_map_get_value_mut(HashMap *self, const byte *key);
// Returns true if there was a value for that key
extern bool hash_map_delete(HashMap *self, const byte *key);

extern HashMapIter hash_map_iter(const HashMap *self);
extern HashMapIterMut hash_map_iter_mut(HashMap *self);

extern void hash_map_clean(HashMap self);

// Iter functions

extern const byte *hash_map_iter_next(HashMapIter *self);
extern byte *hash_map_iter_mut_next(HashMapIterMut *self);

#endif //DUPLICATES_HASH_MAP_H
