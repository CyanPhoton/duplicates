#ifndef DUPLICATES_C_STRING_H
#define DUPLICATES_C_STRING_H

#include "vector.h"

#ifdef STR_INTERNAL
struct CString {
    Vec data;
    size_t str_len;
} typedef CString;
#else
struct CString {
    Vec _data;
    size_t _str_len;
} typedef CString;
#endif

extern CString string_init(c_str str);

extern CString string_join(const CString *self, const CString *other);
extern CString string_extend(const CString *self, c_str other);

// Excludes null-byte
extern size_t string_len(const CString *self);
extern c_str string_c_str(const CString *self);

extern CString string_clone(const CString *self);

extern void string_clean(CString self);

#endif //DUPLICATES_C_STRING_H
