#ifndef DUPLICATES_COMMON_H
#define DUPLICATES_COMMON_H

typedef const char *c_str;
typedef char *c_str_mut;

typedef char byte;

void ptr_destructor(void *ptr);

void *exit_if_null(void *ptr);

#define NOT_NULL(expr) exit_if_null(expr)

#endif //DUPLICATES_COMMON_H
