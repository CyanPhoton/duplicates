#ifndef DUPLICATES_VECTOR_H
#define DUPLICATES_VECTOR_H

#include <stdlib.h>
#include <stdbool.h>
#include "common.h"

typedef void (*VecElementDestructor)(void *element);
typedef void (*VecElementClone)(const void *old, void *new);

extern void no_clone(const void *old, void *new);

#ifdef VEC_INTERNAL
struct Vec {
    byte *data;
    size_t element_size;
    size_t len;
    size_t capacity;
    VecElementDestructor elem_destructor;
    VecElementClone elem_clone;
} typedef Vec;

struct VecIter {
    const byte *data;
    size_t element_size;
    size_t index;
    size_t len;
} typedef VecIter;

struct VecIterMut {
    const byte *data;
    size_t element_size;
    size_t index;
    size_t len;
} typedef VecIterMut;
#else
struct Vec {
    byte* _data;
    size_t _element_size;
    size_t _len;
    size_t _capacity;
    VecElementDestructor _elem_destructor;
    VecElementClone _elem_clone;
} typedef Vec;

struct VecIter {
    const byte* _data;
    size_t _element_size;
    size_t _index;
    size_t _len;
} typedef VecIter;

struct VecIterMut {
    const byte* _data;
    size_t _element_size;
    size_t _index;
    size_t _len;
} typedef VecIterMut;
#endif

// Vec methods

extern Vec vec_init(size_t element_size, VecElementDestructor elem_destructor, VecElementClone elem_clone);
extern Vec vec_with_capacity(size_t element_size, size_t capacity, VecElementDestructor elem_destructor, VecElementClone elem_clone);
extern Vec vec_init_from(size_t element_size, const byte *data, size_t len, VecElementDestructor elem_destructor, VecElementClone elem_clone);
typedef void (*GenFn)(void *data, size_t index, byte *dest);
extern Vec vec_init_fn(size_t element_size, size_t repeat_count, void *data, GenFn fn, VecElementDestructor elem_destructor, VecElementClone elem_clone);

extern void vec_pushback(Vec *self, const byte *element_ptr);
extern void vec_extend(Vec *self, const byte *elements, size_t count);

extern void vec_remove_replace(Vec *self, size_t index);

extern const byte *vec_data(const Vec *self);
extern byte *vec_data_mut(Vec *self);

extern size_t vec_len(const Vec *self);

extern const byte *vec_index(const Vec *self, size_t index);
extern byte *vec_index_mut(Vec *self, size_t index);

extern Vec vec_clone(const Vec *self);

typedef bool (*VecElementEq)(const void *lhs, const void *rhs);
extern size_t contains(const Vec *self, const byte *element, VecElementEq compare);

extern const byte *vec_begin(const Vec *self);
extern byte *vec_begin_mut(Vec *self);
extern const byte *vec_end(const Vec *self);
extern byte *vec_end_mut(Vec *self);

extern VecIter vec_iter(const Vec *self);
extern VecIterMut vec_iter_mut(Vec *self);

extern void vec_clean(Vec self);

// Iter methods

extern const byte *vec_iter_next(VecIter *self);
extern byte *vec_iter_mut_next(VecIterMut *self);

#endif //DUPLICATES_VECTOR_H
