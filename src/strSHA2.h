#ifndef DUPLICATES_STRSHA2_H
#define DUPLICATES_STRSHA2_H

#define	SHA2_DIGEST_LEN_BYTES		32
#define	SHA2_DIGEST_LEN_STR		64

extern char *strSHA2(const char *filename);

#endif //DUPLICATES_STRSHA2_H
