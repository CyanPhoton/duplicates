#define VEC_INTERNAL
#include <memory.h>
#include <math.h>
#include <stdio.h>
#include "vector.h"

#define INIT_CAPACITY 8

Vec vec_init(size_t element_size, VecElementDestructor elem_destructor, VecElementClone elem_clone) {
    Vec result = {.data = NULL, .element_size = element_size, .len = 0, .capacity = 0, .elem_destructor = elem_destructor, .elem_clone = elem_clone};
    return result;
}

Vec vec_with_capacity(size_t element_size, size_t capacity, VecElementDestructor elem_destructor, VecElementClone elem_clone) {
    Vec result = {.data = NULL, .element_size = element_size, .len = 0, .capacity = capacity, .elem_destructor = elem_destructor, .elem_clone = elem_clone};
    result.data = NOT_NULL(malloc(result.capacity * result.element_size));
    return result;
}

Vec vec_init_from(size_t element_size, const byte *data, size_t len, VecElementDestructor elem_destructor, VecElementClone elem_clone) {
    Vec result = {.data = NULL, .element_size = element_size, .len = len, .capacity = len, .elem_destructor = elem_destructor, .elem_clone = elem_clone};
    result.data = NOT_NULL(malloc(result.len * result.element_size));
    memcpy(result.data, data, element_size * len);
    return result;
}

Vec vec_init_fn(size_t element_size, size_t repeat_count, void *data, GenFn fn, VecElementDestructor elem_destructor, VecElementClone elem_clone) {
    Vec result = {.data = NULL, .element_size =element_size, .len = repeat_count, .capacity = repeat_count, .elem_destructor = elem_destructor, .elem_clone = elem_clone};
    result.data = NOT_NULL(malloc(result.len * result.element_size));
    for (size_t i = 0; i < repeat_count; i++) {
        fn(data, i, result.data + (i * element_size));
    }
    return result;
}

void vec_pushback(Vec *self, const byte *element_ptr) {
    if (self->len == self->capacity) {
        if (self->capacity == 0) {
            self->capacity = INIT_CAPACITY;
        } else {
            self->capacity *= 2;
        }

        self->data = NOT_NULL(realloc(self->data, self->capacity * self->element_size));
    }
    memcpy(&self->data[self->len * self->element_size], element_ptr, self->element_size);
    self->len++;
}

void vec_extend(Vec *self, const byte *elements, size_t count) {
    if (self->len + count > self->capacity) {
        if (self->capacity == 0) {
            self->capacity = INIT_CAPACITY;
        } else {
            int mul = (int) pow(2, ceil(log2((double) (self->len + count) / (double) self->capacity)));

            self->capacity *= mul;
        }

        self->data = NOT_NULL(realloc(self->data, self->capacity * self->element_size));
    }
    memcpy(&self->data[self->len * self->element_size], elements, self->element_size * count);
    self->len += count;
}

// Currently, copies last element into removed position so that it's O(1)
// Yes this reorders, but that isn't important for me
void vec_remove_replace(Vec *self, size_t index) {
    if (index < self->len) {
        if (index != self->len - 1) {
            memcpy(vec_index_mut(self, index), vec_index_mut(self, self->len - 1), self->element_size);
        }
        self->len--;
    }
}

const byte *vec_data(const Vec *self) {
    return self->data;
}

byte *vec_data_mut(Vec *self) {
    return self->data;
}

size_t vec_len(const Vec *self) {
    return self->len;
}

const byte *vec_index(const Vec *self, size_t index) {
    if (index < self->len) {
        return &self->data[index * self->element_size];
    } else {
        return NULL;
    }
}

byte *vec_index_mut(Vec *self, size_t index) {
    if (index < self->len) {
        return &self->data[index * self->element_size];;
    } else {
        return NULL;
    }
}

Vec vec_clone(const Vec *self) {
    Vec result = {.data = NOT_NULL(malloc(self->element_size * self->capacity)), .element_size = self->element_size, .len = self->len, .capacity = self->capacity, .elem_destructor = self->elem_destructor, .elem_clone = self->elem_clone};
    if (self->elem_clone == NULL) {
        memcpy(result.data, self->data, self->len * self->element_size);
    } else {
        for (size_t i = 0; i < self->len; i++) {
            self->elem_clone(self->data + (i * self->element_size), result.data + (i * self->element_size));
        }
    }

    return result;
}

size_t contains(const Vec *self, const byte *element, VecElementEq compare) {
    for (size_t i = 0; i < self->len; i++) {
        if (compare == NULL) {
            if (0 == memcmp(vec_index(self, i), element, self->element_size)) {
                return i;
            }
        } else {
            if (compare(vec_index(self, i), element)) {
                return i;
            }
        }
    }

    return -1;
}

const byte *vec_begin(const Vec *self) {
    return self->data;
}

byte *vec_begin_mut(Vec *self) {
    return self->data;
}

const byte *vec_end(const Vec *self) {
    return self->data + (self->len * self->element_size);
}

byte *vec_end_mut(Vec *self) {
    return self->data + (self->len * self->element_size);
}

VecIter vec_iter(const Vec *self) {
    VecIter result = {.data = self->data, .element_size = self->element_size, .index = 0, .len = self->len};
    return result;
}

VecIterMut vec_iter_mut(Vec *self) {
    VecIterMut result = {.data = self->data, .element_size = self->element_size, .index = 0, .len = self->len};
    return result;
}

void vec_clean(Vec self) {
    if (self.elem_destructor != NULL) {
        void *elem = NULL;
        VecIterMut iter = vec_iter_mut(&self);
        while ((elem = vec_iter_mut_next(&iter)) != NULL) {
            self.elem_destructor(elem);
        }
    }
    free(self.data);
}

const byte *vec_iter_next(VecIter *self) {
    if (self->index == self->len) {
        return NULL;
    } else {
        const byte *v = &self->data[self->index * self->element_size];
        self->index++;
        return v;
    }
}

byte *vec_iter_mut_next(VecIterMut *self) {
    if (self->index == self->len) {
        return NULL;
    } else {
        byte *v = (byte *) &self->data[self->index * self->element_size];
        self->index++;
        return v;
    }
}

void no_clone(const void *old, void *new) {
    fprintf(stderr, "Clone disabled");
    exit(EXIT_FAILURE);
}


