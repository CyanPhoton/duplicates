#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <memory.h>
#include "strSHA2.h"
#include "program_arguments.h"
#include "common.h"
#include "path.h"
#include "duplicate_searcher.h"

void search_dir(DIR* dir, Path* dir_path, const ProgramArguments* arguments, DuplicateSearcher* dupe_searcher);
void collect_unique_search_dirs(const ProgramArguments* arguments, Vec* unique_dirs, Vec* unique_dir_paths);

int main(int argc, c_str_mut* argv) {
    const ProgramArguments arguments = arguments_init(argc, argv);

    if (arg_check_option(&arguments, OptionAttemptsAdvanced)) {
        arguments_clean(arguments);
        return EXIT_SUCCESS;
    }

    if (vec_len(arg_get_search_dirs(&arguments)) == 0) {
        fprintf(stderr, "Please provide one or more search directories.\n");
        arguments_clean(arguments);
        return EXIT_FAILURE;
    }

    Vec unique_dirs;
    Vec unique_dir_paths;
    collect_unique_search_dirs(&arguments, &unique_dirs, &unique_dir_paths);

    DuplicateSearcher dupe_searcher = dup_init();

    for (size_t i = 0; i < vec_len(&unique_dirs); i++) {
        DIR** dir = (DIR **) vec_index(&unique_dirs, i);
        Path* path = (Path *) vec_index(&unique_dir_paths, i);

        search_dir(*dir, path, &arguments, &dupe_searcher);
        closedir(*dir);
    }

    vec_clean(unique_dir_paths);
    vec_clean(unique_dirs);


//    printf("Total size: %zu, Largest bin: %zu, Bin index: %zu\n", dupe_searcher._file_records._value_count, dupe_searcher._file_records._largest_bin, dupe_searcher._file_records._bin_count_index);

    if (arg_check_option(&arguments, OptionTargetFile | OptionTargetHash)) {
        c_str target_hash = NULL;
        c_str target_file_path = NULL;
        if (arg_check_option(&arguments, OptionTargetFile)) {
            target_file_path = arg_get_option_input(&arguments, OptionTargetFile);
            target_hash = strSHA2(target_file_path);
        } else {
            target_hash = arg_get_option_input(&arguments, OptionTargetHash);
        }
        DuplicateSummary summary = dup_summarise(&dupe_searcher);

        const DuplicateEntry* dupe_entry = NULL;
        VecIter dupe_entry_iter = vec_iter(&summary.duplicates);
        while ((dupe_entry = (const DuplicateEntry *) vec_iter_next(&dupe_entry_iter)) != NULL) {
            if (0 == strcmp(target_hash, &dupe_entry->sha[0])) {
                const Path *path_entry = NULL;
                VecIter path_entry_iter = vec_iter(&dupe_entry->paths);
                while ((path_entry = (const Path *) vec_iter_next(&path_entry_iter)) != NULL) {
                    if (target_file_path != NULL) {
                        if (0 != strcmp(target_file_path, path_c_str(path_entry))) {
                            printf("%s\n", path_c_str(path_entry));
                        }
                    } else {
                        printf("%s\n", path_c_str(path_entry));
                    }
                }
                break;
            }
        }

        if (target_file_path != NULL) {
            free((void*) target_hash);
        }

        dup_summary_clean(summary);
    } else if (arg_check_option(&arguments, OptionListAllDupe)) {
        DuplicateSummary summary = dup_summarise(&dupe_searcher);

        const DuplicateEntry* dupe_entry = NULL;
        VecIter dupe_entry_iter = vec_iter(&summary.duplicates);
        while ((dupe_entry = (const DuplicateEntry *) vec_iter_next(&dupe_entry_iter)) != NULL) {
            bool first = true;

            const Path* path_entry = NULL;
            VecIter path_entry_iter = vec_iter(&dupe_entry->paths);
            while((path_entry = (const Path *) vec_iter_next(&path_entry_iter)) != NULL) {
                if (first) {
                    first = false;

                    printf("%s", path_c_str(path_entry));
                } else {
                    printf("\t%s", path_c_str(path_entry));
                }
            }
            printf("\n");
        }

        dup_summary_clean(summary);
    } else if (arg_check_option(&arguments, OptionMinimize)) {
        //TODO: extension
        //      Just two loops and a call to create some hardlinks, don't want to potentially mess up files though so not going to do this part
    } else if (arg_check_option(&arguments, OptionSilent)) {
        bool dupe_exits = dup_check(&dupe_searcher);

        dup_clean(dupe_searcher);
        arguments_clean(arguments);

        return dupe_exits ? EXIT_FAILURE : EXIT_SUCCESS;
    } else {
        DuplicateSummary summary = dup_summarise(&dupe_searcher);
        printf("%zu Total files\n", summary.total_files);
        printf("%zu Total size (bytes)\n", summary.total_size);
//        printf("%zu Total allocated size (bytes)\n", summary.total_allocated_size);
        printf("%zu Number of unique\n", summary.total_unique);
        printf("%zu Min possible size (bytes)\n", summary.min_size);
//        printf("%zu Min possible allocated size (bytes)\n", summary.min_allocated_size);

        dup_summary_clean(summary);
    }

    dup_clean(dupe_searcher);
    arguments_clean(arguments);
    return EXIT_SUCCESS;
}

void collect_unique_search_dirs(const ProgramArguments* arguments, Vec* unique_dirs, Vec* unique_dir_paths) {
    Vec search_dir_full_paths = vec_init(sizeof(Path), (VecElementDestructor) path_destructor, (VecElementClone) path_clone_fn);
    Vec search_dir_paths = vec_init(sizeof(Path), (VecElementDestructor) path_destructor, (VecElementClone) path_clone_fn);

    c_str* input_search_dir = NULL;
    VecIter input_search_dir_iter = vec_iter(arg_get_search_dirs(arguments));
    while ((input_search_dir = (c_str *) vec_iter_next(&input_search_dir_iter)) != NULL) {
        struct stat s;
        if (lstat(*input_search_dir, &s) == -1) {
            fprintf(stderr, "Failed to access search directory: %s\n", *input_search_dir);
            perror("\tErr:");
            vec_clean(search_dir_full_paths);
            vec_clean(search_dir_paths);
            arguments_clean(*arguments);
            exit(EXIT_FAILURE);
        }

        if (!S_ISDIR(s.st_mode)) {
            fprintf(stderr, "Argument is not a directory: %s\n", *input_search_dir);
            vec_clean(search_dir_full_paths);
            vec_clean(search_dir_paths);
            arguments_clean(*arguments);
            exit(EXIT_FAILURE);
        }

        c_str full_path = realpath(*input_search_dir, NULL);
        if (full_path == NULL) {
            fprintf(stderr, "Failed to resolve full path for argument: %s\n", *input_search_dir);
            vec_clean(search_dir_full_paths);
            vec_clean(search_dir_paths);
            arguments_clean(*arguments);
            exit(EXIT_FAILURE);
        }

        Path path = path_init(*input_search_dir);
        Path fulL_path_path = path_init(full_path);

        vec_pushback(&search_dir_full_paths, (const byte *) &fulL_path_path);
        vec_pushback(&search_dir_paths, (const byte *) &path);

        free((void*) full_path);
    }

    // Need to remove paths that are children of other paths to prevent duplicates

    Vec unique_dir_full_paths = vec_init(sizeof(Path), (VecElementDestructor) path_destructor, (VecElementClone) path_clone_fn);
    *unique_dir_paths = vec_init(sizeof(Path), (VecElementDestructor) path_destructor, (VecElementClone) path_clone_fn);
    *unique_dirs = vec_init(sizeof(DIR*), NULL, NULL); // Don't clone and manually clean up each one once done with it

    for (size_t i = 0; i < vec_len(&search_dir_full_paths); i++) {
        const Path* path_i = (const Path *) vec_index(&search_dir_full_paths, i);

        bool unique = true;
        for (size_t j = 0; j < vec_len(&search_dir_full_paths); j++) {
            if (i == j) {
                continue;
            }

            const Path* path_j = (const Path *) vec_index(&search_dir_full_paths, j);

            if (path_str_len(path_j) < path_str_len(path_i)) {
                // path_j might be a parent of path_i

                if (0 == strncmp(path_c_str(path_i), path_c_str(path_j), path_str_len(path_j))) {
                    // path_j is a parent of path_i so don't add path_i
                    unique = false;
                    break;
                }
            }
        }

        if (unique) {
            // Still possible that another of the same length that that is identical has already been added to check for it.
            if (-1 == contains(&unique_dir_full_paths, (const byte *) path_i, (VecElementEq) path_compare)) {
                // Truly new

                Path fulL_path_i_clone = path_clone(path_i);
                vec_pushback(&unique_dir_full_paths, (const byte *) &fulL_path_i_clone);

                Path path_i_clone = path_clone((const Path *) vec_index(&search_dir_paths, i));
                vec_pushback(unique_dir_paths, (const byte *) &path_i_clone);

                DIR* unique_search_dir = opendir(path_c_str(&path_i_clone));

                if (unique_search_dir == NULL) {
                    fprintf(stderr, "Failed to open search dir: %s\n", path_c_str(&path_i_clone));
                    perror("\tErr:");

                    vec_clean(unique_dir_full_paths);
                    vec_clean(search_dir_full_paths);
                    vec_clean(search_dir_paths);
                    vec_clean(*unique_dir_paths);
                    vec_clean(*unique_dirs);
                    arguments_clean(*arguments);
                    exit(EXIT_FAILURE);
                }

                vec_pushback(unique_dirs, (const byte *) &unique_search_dir);
            }
        }
    }

    vec_clean(unique_dir_full_paths);
    vec_clean(search_dir_full_paths);
    vec_clean(search_dir_paths);
}

void search_dir(DIR* dir, Path* dir_path, const ProgramArguments* arguments, DuplicateSearcher* dupe_searcher) {
    struct dirent* entry = NULL;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_name[0] != '.' || arg_check_option(arguments,OptionIncludeHidden)) {
            if (entry->d_name[0] == '.' && (entry->d_name[1] == '\0' || (entry->d_name[1] == '.' && entry->d_name[2] == '\0'))) {
                // ignore `.` and `..` files

                Path new_path = path_extend(dir_path, &entry->d_name[0]);
                struct stat s;
                lstat(path_c_str(&new_path), &s);

            } else {
                Path new_path = path_extend(dir_path, &entry->d_name[0]);

                struct stat s;
                lstat(path_c_str(&new_path), &s);

                switch (s.st_mode & S_IFMT) {
                    case S_IFLNK: {
                        break;
                    }
                    case S_IFDIR: {
                        DIR* new_dir = opendir(path_c_str(&new_path));
                        if (new_dir != NULL) {
                            search_dir(new_dir, &new_path, arguments, dupe_searcher);
                        }
                        closedir(new_dir);
                        break;
                    }
                    case S_IFREG: {
                        dup_process_file(dupe_searcher, s.st_size, s.st_blksize * s.st_blocks, path_clone(&new_path), s.st_ino); //TODO: need to cleanup this clone
                        break;
                    }
                }

                path_clean(new_path);
            }
        }
    }
}
