#define HASH_SET_INTERNAL
#include "hash_set.h"

const byte *set_key_accessor(const void *value) {
    return value;
}

HashSet hash_set_init(size_t key_size, VecElementDestructor value_destructor) {
    HashSet result = {.hashMap = hash_map_init(key_size, key_size, set_key_accessor, value_destructor)};
    return result;
}

bool hash_set_insert(HashSet *self, const byte *key) {
    return hash_map_insert(&self->hashMap, key);
}

void hash_set_clean(HashSet self) {
    hash_map_clean(self.hashMap);
}
