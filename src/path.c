#define PATH_INTERNAL
#include <string.h>
#include "path.h"

Path path_init(c_str str) {
    size_t len = strlen(str);

    Path result = {.data = vec_init_from(sizeof(char), str, len + 1, NULL, NULL), .str_len = len};
    return result;
}

Path path_init_string(CString str) {
    Path result = {.data = vec_init_from(sizeof(char), string_c_str(&str), string_len(&str) + 1, NULL, NULL), .str_len = string_len(&str)};
    return result;
}

Path path_join(const Path *self, const Path *other) {
    char sep = '/';
    int needs_sep = *vec_index(&self->data, self->str_len - 1) != sep ? 1 : 0;

    Path result = {.data = vec_with_capacity(sizeof(char), self->str_len + other->str_len + 1 + needs_sep, NULL, NULL), .str_len = self->str_len + other->str_len + needs_sep};

    vec_extend(&result.data, path_c_str(self), self->str_len);

    if (needs_sep == 1) {
        vec_pushback(&result.data, &sep);
    }

    vec_extend(&result.data, path_c_str(other), other->str_len);

    char null = '\0';
    vec_pushback(&result.data, &null);

    return result;
}

Path path_extend(const Path *self, c_str other) {
    char sep = '/';
    int needs_sep = *vec_index(&self->data, self->str_len - 1) != sep ? 1 : 0;

    size_t other_len = strlen(other);

    Path result = {.data = vec_with_capacity(sizeof(char), self->str_len + other_len + 1 + needs_sep, NULL, NULL), .str_len = self->str_len + other_len + needs_sep};

    vec_extend(&result.data, path_c_str(self), self->str_len);

    if (needs_sep == 1) {
        vec_pushback(&result.data, &sep);
    }

    vec_extend(&result.data, other, other_len);

    char null = 0;
    vec_pushback(&result.data, &null);

    return result;
}

size_t path_str_len(const Path *self) {
    return self->str_len;
}

c_str path_c_str(const Path *self) {
    return vec_data(&self->data);
}

Path path_clone(const Path *self) {
    Path result = {.data = vec_clone(&self->data), .str_len = self->str_len};
    return result;
}

void path_clean(Path self) {
    vec_clean(self.data);
}

void path_destructor(Path *self) {
    vec_clean(self->data);
}

void path_clone_fn(const Path *old, Path *new) {
    *new = path_clone(old);
}

bool path_compare(const Path *lhs, const Path *rhs) {
    return lhs->str_len == rhs->str_len
           && 0 == strcmp(path_c_str(lhs), path_c_str(rhs));
}
