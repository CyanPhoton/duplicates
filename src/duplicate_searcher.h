#ifndef DUPLICATES_DUPLICATE_SEARCHER_H
#define DUPLICATES_DUPLICATE_SEARCHER_H

#include <stddef.h>
#include "path.h"
#include "hash_map.h"
#include "strSHA2.h"

#ifdef DUP_INTERNAL
struct DuplicateSearcher {
    HashMap file_records;
} typedef DuplicateSearcher;
#else
struct DuplicateSearcher {
    HashMap _file_records;
} typedef DuplicateSearcher;
#endif

struct DuplicateSummary {
    size_t total_files;
    size_t total_size;
    size_t total_allocated_size;
    size_t total_unique;
    size_t min_size;
    size_t min_allocated_size;
    Vec duplicates;
} typedef DuplicateSummary;

struct DuplicateEntry {
    char sha[SHA2_DIGEST_LEN_STR + 1];
    Vec paths;
} typedef DuplicateEntry;

extern DuplicateSearcher dup_init();

extern void dup_process_file(DuplicateSearcher *self, size_t file_size, size_t allocated_size, Path file_path, __ino_t inode);

extern DuplicateSummary dup_summarise(DuplicateSearcher *self);
// Returns true if any duplicates are found
extern bool dup_check(DuplicateSearcher *self);

extern void dup_clean(DuplicateSearcher self);

extern void dup_summary_clean(DuplicateSummary self);

#endif //DUPLICATES_DUPLICATE_SEARCHER_H
