#ifndef DUPLICATES_PROGRAM_ARGUMENTS_H
#define DUPLICATES_PROGRAM_ARGUMENTS_H

#include <stdbool.h>
#include "vector.h"

enum Option {
    OptionNone = 0,
    OptionAttemptsAdvanced = 1 << 0,
    OptionIncludeHidden = 1 << 1,
    OptionTargetFile = 1 << 2,
    OptionTargetHash = 1 << 3,
    OptionListAllDupe = 1 << 4,
    OptionMinimize = 1 << 5,
    OptionSilent = 1 << 6,
} typedef Option;

#ifdef ARGS_INTERNAL
struct ProgramArguments {
    int options;
    const char *file_name_input;
    const char *hash_input;
    Vec search_directories;
} typedef ProgramArguments;
#else
struct ProgramArguments {
    int _options;
    const char* _file_name_input;
    const char* _hash_input;
    Vec _search_directories;
} typedef ProgramArguments;
#endif

extern ProgramArguments arguments_init(int argc, c_str_mut argv[]);
extern bool arg_check_option(const ProgramArguments *self, Option option);
extern c_str arg_get_option_input(const ProgramArguments *self, Option option);
extern const Vec *arg_get_search_dirs(const ProgramArguments *self);
extern void arguments_clean(ProgramArguments self);

#endif //DUPLICATES_PROGRAM_ARGUMENTS_H
