#define HASH_MAP_INTERNAL
#include <memory.h>
#include <math.h>
#include <stdint.h>
#include "hash_map.h"

#define DENSITY_THRESHOLD 0.5
#define BIN_SIZE_THRESHOLD 10

#define PRIME_COUNT 11
// A set of primes where each is >= 2 * the prior
size_t PRIMES[PRIME_COUNT] = {1009, 2027, 4057, 8117, 16249, 32503, 65011, 130027, 260081, 520193, 1040387};

struct HashMapBin {
    Vec entries;
} typedef HashMapBin;

struct HashBinGenData {
    size_t element_size;
    VecElementDestructor value_destructor;
} typedef HashBinGenData;

size_t bin_count(const HashMap *self);
size_t bin_count_from_index(size_t index);
void potentially_increase_bin_size(HashMap *self);
size_t hash_algo(const byte *data, size_t size);

void bins_destructor(HashMapBin *bin) {
    vec_clean(bin->entries);
}

void hash_bin_gen(HashBinGenData *data, size_t i, HashMapBin *dest) {
    HashMapBin bin = {.entries = vec_init(data->element_size, data->value_destructor, NULL)}; // Null since no clone
    *dest = bin;
}

HashMap hash_map_init(size_t key_size, size_t value_size, KeyAccessor key_accessor, VecElementDestructor value_destructor) {
    HashBinGenData gen_data = {.element_size = value_size, .value_destructor = value_destructor};
    HashMap result = {.bins = vec_init_fn(sizeof(HashMapBin), bin_count_from_index(0), &gen_data, (GenFn) hash_bin_gen, (VecElementDestructor) bins_destructor,
                                          NULL), .key_size = key_size, .value_size = value_size, .bin_count_index = 0, .value_count = 0, key_accessor = key_accessor, .largest_bin = 0, .value_destructor = value_destructor};
    return result;
}

HashMap hash_map_init_with_bin(size_t key_size, size_t value_size, KeyAccessor key_accessor, size_t bin_index, VecElementDestructor value_destructor) {
    HashBinGenData gen_data = {.element_size = value_size, .value_destructor = value_destructor};
    HashMap result = {.bins = vec_init_fn(sizeof(HashMapBin), bin_count_from_index(bin_index), &gen_data, (GenFn) hash_bin_gen, (VecElementDestructor) bins_destructor,
                                          NULL), .key_size = key_size, .value_size = value_size, .bin_count_index = bin_index, .value_count = 0, key_accessor = key_accessor, .largest_bin = 0, .value_destructor = value_destructor};
    return result;
}

bool hash_map_insert(HashMap *self, const byte *value) {
    potentially_increase_bin_size(self);

    const byte *key = self->key_accessor(value);
    size_t hash = hash_algo(key, self->key_size);
    size_t bin_i = hash % bin_count(self);

    Vec *bin = &((HashMapBin *) vec_index_mut(&self->bins, bin_i))->entries;

    bool entry_found = false;
    byte *value_entry = NULL;
    VecIterMut iter = vec_iter_mut(bin);
    while ((value_entry = vec_iter_mut_next(&iter)) != NULL) {
        const byte *other_key = self->key_accessor(value_entry);

        if (memcmp(key, other_key, self->key_size) == 0) {
            memcpy(value_entry, value, self->value_size);
            entry_found = true;
            break;
        }
    }

    if (!entry_found) {
        vec_pushback(bin, value);
        size_t new_len = vec_len(bin);
        self->largest_bin = new_len > self->largest_bin ? new_len : self->largest_bin;
    }

    self->value_count++;

    return entry_found;
}

const byte *hash_map_get_value(const HashMap *self, const byte *key) {
    size_t hash = hash_algo(key, self->key_size);
    size_t bin_i = hash % bin_count(self);

    const Vec *bin = &((HashMapBin *) vec_index(&self->bins, bin_i))->entries;

    const byte *value_entry = NULL;
    VecIter iter = vec_iter(bin);
    while ((value_entry = vec_iter_next(&iter)) != NULL) {
        const byte *other_key = self->key_accessor(value_entry);

        if (memcmp(key, other_key, self->key_size) == 0) {
            return value_entry;
        }
    }

    return NULL;
}

byte *hash_map_get_value_mut(HashMap *self, const byte *key) {
    size_t hash = hash_algo(key, self->key_size);
    size_t bin_i = hash % bin_count(self);

    Vec *bin = &((HashMapBin *) vec_index_mut(&self->bins, bin_i))->entries;

    byte *value_entry = NULL;
    VecIterMut iter = vec_iter_mut(bin);
    while ((value_entry = vec_iter_mut_next(&iter)) != NULL) {
        const byte *other_key = self->key_accessor(value_entry);

        if (memcmp(key, other_key, self->key_size) == 0) {
            return value_entry;
        }
    }

    return NULL;
}

bool hash_map_delete(HashMap *self, const byte *key) {
    size_t hash = hash_algo(key, self->key_size);
    size_t bin_i = hash % bin_count(self);

    Vec *bin = &((HashMapBin *) vec_index_mut(&self->bins, bin_i))->entries;

    size_t i = 0;
    byte *value_entry = NULL;
    VecIterMut iter = vec_iter_mut(bin);
    while ((value_entry = vec_iter_mut_next(&iter)) != NULL) {
        const byte *other_key = self->key_accessor(value_entry);

        if (memcmp(key, other_key, self->key_size) == 0) {
            vec_remove_replace(bin, i);

            self->value_count--;

            return true;
        }

        i++;
    }

    return false;
}

HashMapIter hash_map_iter(const HashMap *self) {
    VecIter bins = vec_iter(&self->bins);
    const HashMapBin *first_bin = (const HashMapBin *) vec_iter_next(&bins);
    VecIter entries = vec_iter(&first_bin->entries);

    HashMapIter result = {.hashMap = self, .bins = bins, .entries = entries};
    return result;
}

HashMapIterMut hash_map_iter_mut(HashMap *self) {
    VecIterMut bins = vec_iter_mut(&self->bins);
    HashMapBin *first_bin = (HashMapBin *) vec_iter_mut_next(&bins);
    VecIterMut entries = vec_iter_mut(&first_bin->entries);

    HashMapIterMut result = {.hashMap = self, .bins = bins, .entries = entries};
    return result;
}

void hash_map_clean(HashMap self) {
    vec_clean(self.bins);
}

size_t bin_count(const HashMap *self) {
    return bin_count_from_index(self->bin_count_index);
}

size_t bin_count_from_index(size_t index) {
    if (index < PRIME_COUNT) {
        return PRIMES[index];
    } else {
        return PRIMES[PRIME_COUNT - 1] * (int) pow(2.0, (double) (index - PRIME_COUNT + 1));
    }
}

void potentially_increase_bin_size(HashMap *self) {
    if (((double) self->value_count / (double) bin_count(self)) < DENSITY_THRESHOLD && self->largest_bin < BIN_SIZE_THRESHOLD) {
        return;
    }

    HashMap new_hash_map = hash_map_init_with_bin(self->key_size, self->value_size, self->key_accessor, self->bin_count_index + 1, self->value_destructor);

    HashMapIter old_iter = hash_map_iter(self);
    const byte *old_value = NULL;
    while ((old_value = hash_map_iter_next(&old_iter)) != NULL) {
        hash_map_insert(&new_hash_map, old_value);
    }

    *self = new_hash_map;
}

// Since primarily going to be used for size_t, use fnv algo
// http://www.isthe.com/chongo/tech/comp/fnv/index.html -> http://www.isthe.com/chongo/src/fnv/fnv.h & http://www.isthe.com/chongo/src/fnv/hash_64.c
size_t hash_algo(const byte *data, size_t size) {
    uint64_t hval = 0xcbf29ce484222325ULL;

    const unsigned char *bp = (const unsigned char*) data;
    const unsigned char *be = (const unsigned char*) (data + size);

    while (bp < be) {
        hval += (hval << 1) + (hval << 4) + (hval << 5) +
                (hval << 7) + (hval << 8) + (hval << 40);

        hval ^= (uint64_t) *bp++;
    }

    return hval;
}

// Iter functions

const byte *hash_map_iter_next(HashMapIter *self) {
    const byte *next = vec_iter_next(&self->entries);
    while (next == NULL) {
        const HashMapBin *next_bin = (const HashMapBin *) vec_iter_next(&self->bins);
        if (next_bin == NULL) {
            return NULL;
        }
        self->entries = vec_iter(&next_bin->entries);
        next = vec_iter_next(&self->entries);
    }

    return next;
}

byte *hash_map_iter_mut_next(HashMapIterMut *self) {
    byte *next = vec_iter_mut_next(&self->entries);
    while (next == NULL) {
        HashMapBin *next_bin = (HashMapBin *) vec_iter_mut_next(&self->bins);
        if (next_bin == NULL) {
            return NULL;
        }
        self->entries = vec_iter_mut(&next_bin->entries);
        next = vec_iter_mut_next(&self->entries);
    }

    return next;
}
