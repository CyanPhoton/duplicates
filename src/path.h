#ifndef DUPLICATES_PATH_H
#define DUPLICATES_PATH_H

#include "vector.h"
#include "c_string.h"

#ifdef PATH_INTERNAL
struct Path {
    Vec data;
    size_t str_len;
} typedef Path;
#else
struct Path {
    Vec _data;
    size_t _str_len;
} typedef Path;
#endif

extern Path path_init(c_str str);
extern Path path_init_string(CString str);

extern Path path_join(const Path *self, const Path *other);
extern Path path_extend(const Path *self, c_str other);

// Excludes null-byte
extern size_t path_str_len(const Path *self);
extern c_str path_c_str(const Path *self);

extern Path path_clone(const Path *self);

extern void path_clean(Path self);

extern void path_destructor(Path *self);
extern void path_clone_fn(const Path *old, Path *new);
extern bool path_compare(const Path *lhs, const Path *rhs);

#endif //DUPLICATES_PATH_H
