#include <stdlib.h>
#include <stdio.h>
#include "common.h"

void ptr_destructor(void *ptr) {
    free(ptr);
}

void *exit_if_null(void *ptr) {
    if (ptr == NULL) {
        fprintf(stderr, "Allocation failed, exiting");
        exit(1);
    }
    return ptr;
}
