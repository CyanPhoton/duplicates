#define DUP_INTERNAL
#include <memory.h>
#include <unistd.h>
#include <pthread.h>
#include "duplicate_searcher.h"
#include "hash_set.h"

struct HashMapEntry {
    size_t file_size;
    size_t allocated_size;
    Vec files;
} typedef HashMapEntry;

HashMapEntry hash_map_entry_clone(const HashMapEntry *self) {
    HashMapEntry result = {.file_size = self->file_size, .allocated_size = self->allocated_size, .files = vec_clone(&self->files)};
    return result;
}

void hash_map_entry_destructor(HashMapEntry *entry) {
    vec_clean(entry->files);
}

const size_t *dupe_key_accessor(HashMapEntry *value) {
    return &value->file_size;
}

struct FilesEntry {
    Path file_path;
    __ino_t inode;
} typedef FilesEntry;

void files_entry_destructor(FilesEntry *entry) {
    path_clean(entry->file_path);
}

void files_entry_clone(const FilesEntry *old, FilesEntry *new) {
    new->inode = old->inode;
    new->file_path = path_clone(&old->file_path);
}

DuplicateSearcher dup_init() {
    DuplicateSearcher result = {.file_records = hash_map_init(sizeof(size_t), sizeof(HashMapEntry), (KeyAccessor) dupe_key_accessor, (VecElementDestructor) hash_map_entry_destructor)};
    return result;
}

void dup_process_file(DuplicateSearcher *self, size_t file_size, size_t allocated_size, Path file_path, __ino_t inode) {
    FilesEntry file_entry = {.file_path = file_path, .inode = inode};

    HashMapEntry *size_entry = (HashMapEntry *) hash_map_get_value_mut(&self->file_records, (const byte *) &file_size);
    if (size_entry == NULL) {

        HashMapEntry size_entry = {.file_size = file_size, .allocated_size = allocated_size, .files = vec_init_from(sizeof(FilesEntry), (const byte *) &file_entry, 1, (VecElementDestructor) files_entry_destructor,
                                                                                                                    (VecElementClone) files_entry_clone)};

        hash_map_insert(&self->file_records, (const byte *) &size_entry);
    } else {

        vec_pushback(&size_entry->files, (const byte *) &file_entry);
    }
}

const char *dup_entry_key_accessor(const DuplicateEntry *value) {
    return &value->sha[0];
}

void dup_entry_destructor(DuplicateEntry *entry) {
    vec_clean(entry->paths);
}

void dup_entry_clone(const DuplicateEntry *old, DuplicateEntry *new) {
    memcpy(&new->sha[0], &old->sha[0], SHA2_DIGEST_LEN_STR + 1);
    new->paths = vec_clone(&old->paths);
}

struct DupSummaryThreadInput {
    size_t total_files;
    size_t total_size;
    size_t total_allocated_size;
    size_t total_unique;
    size_t min_size;
    size_t min_allocated_size;
    Vec duplicates;
    Vec entries;
} typedef DupSummaryThreadInput;

void dup_summary_thread_input_gen(void *data, size_t index, DupSummaryThreadInput *dest) {
    DupSummaryThreadInput input = {0};
    input.duplicates = vec_init(sizeof(DuplicateEntry), (VecElementDestructor) dup_entry_destructor, (VecElementClone) dup_entry_clone);
    input.entries = vec_init(sizeof(HashMapEntry), (VecElementDestructor) hash_map_entry_destructor, no_clone);
    *dest = input;
}

void dup_summary_thread_input_destroyer(DupSummaryThreadInput *self) {
    vec_clean(self->duplicates);
}

void file_entry_to_path_clone(const Vec *files, size_t index, Path *dest) {
    FilesEntry *entry = (FilesEntry *) vec_index(files, index);
    path_clone_fn(&entry->file_path, dest);
}

void dup_summarise_thread(DupSummaryThreadInput *input) {
    HashMapEntry *size_entry = NULL;
    VecIter size_entry_iter = vec_iter(&input->entries);
    while ((size_entry = (HashMapEntry *) vec_iter_next(&size_entry_iter)) != NULL) {
        size_t file_count = vec_len(&size_entry->files);
        input->total_files += file_count;
        if (size_entry->file_size == 0) {
            input->total_unique += 1;

            if (file_count > 1) {
                DuplicateEntry new_entry = {.paths = vec_init_fn(sizeof(Path), file_count, &size_entry->files, (GenFn) file_entry_to_path_clone, (VecElementDestructor) path_destructor, (VecElementClone) path_clone_fn)};

                vec_pushback(&input->duplicates, (const byte *) &new_entry);
            }
        } else if (file_count == 1) {
            input->total_unique += 1;
            input->min_size += size_entry->file_size;
            input->min_allocated_size += size_entry->allocated_size;
            input->total_size += size_entry->file_size;
            input->total_allocated_size += size_entry->allocated_size;
        } else {
            HashMap sha_results = hash_map_init(SHA2_DIGEST_LEN_STR + 1, sizeof(DuplicateEntry), (KeyAccessor) dup_entry_key_accessor, (VecElementDestructor) dup_entry_destructor);
            HashSet inodes = hash_set_init(sizeof(__ino_t), NULL);

            const FilesEntry *files_entry = NULL;
            VecIter files_iter = vec_iter(&size_entry->files);
            while ((files_entry = (const FilesEntry *) vec_iter_next(&files_iter)) != NULL) {
                const char *sha = strSHA2(path_c_str(&files_entry->file_path));

                if (sha == NULL) {
                    input->total_files--;
                } else {
                    if (!hash_set_insert(&inodes, (const byte *) &files_entry->inode)) {
                        // Inode not already added, so can add to total size
                        input->total_size += size_entry->file_size;
                        input->total_allocated_size += size_entry->allocated_size;
                    }

                    DuplicateEntry *dup_entry = (DuplicateEntry *) hash_map_get_value_mut(&sha_results, sha);
                    if (dup_entry == NULL) {
                        Path file_path_clone = path_clone(&files_entry->file_path);

                        DuplicateEntry dup_entry = {.paths = vec_init_from(sizeof(Path), (const byte *) &file_path_clone, 1, (VecElementDestructor) path_destructor, (VecElementClone) path_clone_fn)};
                        memcpy(dup_entry.sha, sha, SHA2_DIGEST_LEN_STR + 1);

                        hash_map_insert(&sha_results, (const byte *) &dup_entry);

                        // Did not already exist
                        input->total_unique += 1;
                        input->min_size += size_entry->file_size;
                        input->min_allocated_size += size_entry->allocated_size;
                    } else {
                        Path file_path_clone = path_clone(&files_entry->file_path);
                        vec_pushback(&dup_entry->paths, (const byte *) &file_path_clone);
                    }
                    free((void *) sha);
                }
            }

            const DuplicateEntry *sha_result_entry = NULL;
            HashMapIter sha_result_iter = hash_map_iter(&sha_results);
            while ((sha_result_entry = (const DuplicateEntry *) hash_map_iter_next(&sha_result_iter)) != NULL) {
                if (vec_len(&sha_result_entry->paths) > 1) {
                    DuplicateEntry new_entry = {.paths = vec_clone(&sha_result_entry->paths)};
                    memcpy(&new_entry.sha[0], &sha_result_entry->sha[0], SHA2_DIGEST_LEN_STR + 1);

                    vec_pushback(&input->duplicates, (const byte *) &new_entry);
                }
            }

            hash_map_clean(sha_results);
        }
    }
}

void thread_joiner(const pthread_t *thread) {
    pthread_join(*thread, NULL);
}

DuplicateSummary dup_summarise(DuplicateSearcher *self) {
    size_t total_files = 0;
    size_t total_size = 0;
    size_t total_allocated_size = 0;
    size_t total_unique = 0;
    size_t min_size = 0;
    size_t min_allocated_size = 0;
    Vec duplicates = vec_init(sizeof(DuplicateEntry), (VecElementDestructor) dup_entry_destructor, (VecElementClone) dup_entry_clone);

    long number_of_cores = sysconf(_SC_NPROCESSORS_ONLN);

    Vec thread_inputs = vec_init_fn(sizeof(DupSummaryThreadInput), number_of_cores, NULL, (GenFn) dup_summary_thread_input_gen, (VecElementDestructor) dup_summary_thread_input_destroyer, no_clone);
    Vec threads = vec_with_capacity(sizeof(pthread_t), number_of_cores, (VecElementDestructor) thread_joiner, no_clone);

    {
        size_t i = 0;
        const HashMapEntry *size_entry = NULL;
        HashMapIter size_iter = hash_map_iter(&self->file_records);
        while ((size_entry = (const HashMapEntry *) hash_map_iter_next(&size_iter)) != NULL) {
            DupSummaryThreadInput *input = (DupSummaryThreadInput *) vec_index(&thread_inputs, i);

            HashMapEntry size_entry_clone = hash_map_entry_clone(size_entry);
            vec_pushback(&input->entries, (const byte *) &size_entry_clone);

            i++;
            i %= number_of_cores;
        }
    }

    for (int i = 0; i < number_of_cores; i++) {
        DupSummaryThreadInput *input = (DupSummaryThreadInput *) vec_index(&thread_inputs, i);
        pthread_t thread;
        pthread_create(&thread, NULL, (void *(*)(void *)) dup_summarise_thread, input);
        vec_pushback(&threads, (const byte *) &thread);
    }

    vec_clean(threads); // Will join each

    for (int i = 0; i < number_of_cores; i++) {
        DupSummaryThreadInput *input = (DupSummaryThreadInput *) vec_index(&thread_inputs, i);
        total_files += input->total_files;
        total_size += input->total_size;
        total_allocated_size += input->total_allocated_size;
        total_unique += input->total_unique;
        min_size += input->min_size;
        min_allocated_size += input->min_allocated_size;

        for (int j = 0; j < vec_len(&input->duplicates); j++) {
            DuplicateEntry *dup_entry = (DuplicateEntry *) vec_index(&input->duplicates, j);
            DuplicateEntry dup_entry_cloned = {0};
            dup_entry_clone(dup_entry, &dup_entry_cloned);

            vec_pushback(&duplicates, (const byte *) &dup_entry_cloned);
        }

        vec_clean(input->duplicates);
        vec_clean(input->entries);
    }

    DuplicateSummary result = {.total_files = total_files, .total_size = total_size, .total_allocated_size = total_allocated_size, .total_unique = total_unique, .min_size = min_size, .min_allocated_size = min_allocated_size, .duplicates = duplicates};
    return result;
}

bool dup_check(DuplicateSearcher *self) {
    const HashMapEntry *size_entry = NULL;
    HashMapIter size_iter = hash_map_iter(&self->file_records);
    while ((size_entry = (const HashMapEntry *) hash_map_iter_next(&size_iter)) != NULL) {
        if (size_entry->file_size == 0) {
            //TODO All 0 zero just be ignored?
        } else {
            size_t file_count = vec_len(&size_entry->files);

            if (file_count != 1) {
                HashSet sha_results = hash_set_init(SHA2_DIGEST_LEN_STR + 1, NULL);

                const FilesEntry *files_entry = NULL;
                VecIter files_iter = vec_iter(&size_entry->files);
                while ((files_entry = (const FilesEntry *) vec_iter_next(&files_iter)) != NULL) {
                    const char *sha = strSHA2(path_c_str(&files_entry->file_path));

                    if (sha != NULL) {
                        if (hash_set_insert(&sha_results, sha)) {
                            free((void *) sha);
                            hash_set_clean(sha_results);
                            // Did already exist
                            return true;
                        }
                    }
                    free((void *) sha);
                }

                hash_set_clean(sha_results);
            }
        }
    }

    return false;
}

void dup_clean(DuplicateSearcher self) {
    hash_map_clean(self.file_records);
}

void dup_summary_clean(DuplicateSummary self) {
    vec_clean(self.duplicates);
}
