#ifndef DUPLICATES_HASH_SET_H
#define DUPLICATES_HASH_SET_H

#include "hash_map.h"

#ifdef HASH_SET_INTERNAL
struct HashSet {
    HashMap hashMap;
} typedef HashSet;
#else
struct HashSet {
    HashMap _hashMap;
} typedef HashSet;
#endif

extern HashSet hash_set_init(size_t key_size, VecElementDestructor value_destructor);

// Returns true if there was already the same key
extern bool hash_set_insert(HashSet *self, const byte *key);

extern void hash_set_clean(HashSet self);

#endif //DUPLICATES_HASH_SET_H
