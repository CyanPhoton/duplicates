#define STR_INTERNAL
#include <string.h>
#include "c_string.h"

CString string_init(c_str str) {
    size_t len = strlen(str);

    CString result = {.data = vec_init_from(sizeof(char), str, len + 1, NULL, NULL), .str_len = len};
    return result;
}

CString string_join(const CString *self, const CString *other) {
    CString result = {.data = vec_with_capacity(sizeof(char), self->str_len + other->str_len + 1, NULL, NULL), .str_len = self->str_len + other->str_len};
    vec_extend(&result.data, string_c_str(self), self->str_len);
    vec_extend(&result.data, string_c_str(other), other->str_len);
    char null = '\0';
    vec_pushback(&result.data, &null);

    return result;
}

CString string_extend(const CString *self, c_str other) {
    size_t other_len = strlen(other);

    CString result = {.data = vec_with_capacity(sizeof(char), self->str_len + other_len + 1, NULL, NULL), .str_len = self->str_len + other_len};
    vec_extend(&result.data, string_c_str(self), self->str_len);
    vec_extend(&result.data, other, other_len);
    char null = 0;
    vec_pushback(&result.data, &null);

    return result;
}

size_t string_len(const CString *self) {
    return self->str_len;
}

c_str string_c_str(const CString *self) {
    return vec_data(&self->data);
}

CString string_clone(const CString *self) {
    CString result = {.data = vec_clone(&self->data), .str_len = self->str_len};
    return result;
}

void string_clean(CString self) {
    vec_clean(self.data);
}
