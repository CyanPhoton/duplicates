#include <getopt.h>
#include <stddef.h>
#include <stdio.h>
#define ARGS_INTERNAL
#include "program_arguments.h"

ProgramArguments arguments_init(int argc, c_str_mut argv[]) {
    ProgramArguments a = {.options = OptionNone, .file_name_input = NULL, .hash_input = NULL, .search_directories = vec_init(sizeof(const char *), NULL, NULL)};

    int result;
    while ((result = getopt(argc, argv, "Aaf:h:lmq")) != -1) {
        switch (result) {
            case 'A': {
                a.options |= OptionAttemptsAdvanced;
                break;
            }
            case 'a': {
                a.options |= OptionIncludeHidden;
                break;
            }
            case 'f': {
                a.options |= OptionTargetFile;
                a.file_name_input = optarg;
                break;
            }
            case 'h': {
                a.options |= OptionTargetHash;
                a.hash_input = optarg;
                break;
            }
            case 'l': {
                a.options |= OptionListAllDupe;
                break;
            }
            case 'm': {
                a.options |= OptionMinimize;
                break;
            }
            case 'q': {
                a.options |= OptionSilent;
                break;
            }
            default: {
                fprintf(stderr, "Invalid command option: %c", result);
                exit(EXIT_FAILURE);
                break;
            }
        }
    }

    for (size_t i = optind; i < argc; i++) {
        vec_pushback(&a.search_directories, (byte *) &argv[i]);
    }

    return a;
}

bool arg_check_option(const ProgramArguments *self, Option option) {
    if (option == OptionNone) {
        return self->options == OptionNone;
    } else {
        return self->options & option;
    }
}

c_str arg_get_option_input(const ProgramArguments *self, Option option) {
    if (option & OptionTargetFile) {
        return self->file_name_input;
    } else if (option & OptionTargetHash) {
        return self->hash_input;
    } else {
        return NULL;
    }
}

const Vec *arg_get_search_dirs(const ProgramArguments *self) {
    return &self->search_directories;
}

void arguments_clean(ProgramArguments self) {
    vec_clean(self.search_directories);
}
