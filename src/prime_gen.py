import requests

url = 'https://primes.utm.edu/lists/small/100000.txt'

r = requests.get(url, allow_redirects=True)

choice_primes = []

# Doesn't need to be init prime as finds next from after that is >= 2 * it
last_prime = 500

found_blank = False
for l in r.content.splitlines():
    s = l.decode('utf-8')

    if found_blank:
        for n in s.split():
            if n.isnumeric():
                m = int(n)
                if m > 2 * last_prime:
                    last_prime = m
                    print(m)
                    choice_primes.append(m)

    if len(s) == 0:
        found_blank = True
